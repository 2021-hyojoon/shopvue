
class Address {
    constructor(zipCode, zipAddress, streetAddress, country){
        this.zipCode = zipCode,
        this.zipAddress = zipAddress,
        this.streetAddress = streetAddress,
        this.country = country
    }
}

export default Address;