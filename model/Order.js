
class Order {
    constructor(email, name, nickName, phoneNumber, birthDay, addresses, membershipList){
        this.email = email;
        this.name = name;
        this.nickName = nickName;
        this.phoneNumber = phoneNumber;
        this.birthDay = birthDay;
        this.addresses = addresses;
        this.membershipList = membershipList;
    }
}

export default Order;