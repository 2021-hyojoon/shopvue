
class Product {
    constructor(productName, price, count, intro, category, image){
        this.productName = productName,
        this.price = price,
        this.count = count,
        this.intro = intro,
        this.category = category,
        this.image = image
    }
}

export default Product;