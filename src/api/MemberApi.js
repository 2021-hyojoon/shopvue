import axios from 'axios';

export const findUser = (id, password) => {
    return axios.get(`/member/find?id=${id}&password=${password}`).then(
        result => result.data
    );
}

export const findById = (id) => {
    //
    return axios.get(`/member/find?id=${id}`).then(result => result.data);
}

export const registerUser = (member) => {
    axios.post("/member/register", member).then(
        result => (result.status === 201)
    )
}

export const modifyUser = (user) => {
    axios.put("/member/modify", user);
}

export const deleteUser = (id) => {
    axios.delete(`/member/remove?id=${id}`).then(result => result.data);
}