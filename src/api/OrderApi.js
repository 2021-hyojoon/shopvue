import axios from 'axios';


export const findOrders = () => {
    return axios.get("/order/find-all").then(
        result => result.data
    )
}

export const findOrder = (id) => {
    return axios.get(`/order/find?orderId=${id}`).then(
        result => result.data
    )
}

export const registerOrder = (order) => {
    axios.post("/order/register", order).then(
        result => (result.status === 201)
    )
}

export const deleteOrder = (id) => {
    axios.delete(`/order/remove?orderId=${id}`);
}

export const modifyOrder = (order) => {
    axios.put("/order/modify", order);
}