import axios from 'axios';

export const registerProduct = (product) => {
    axios.post("/product/register", product).then(
        result => (result.status === 201)
    )
}

export const findProducts = () => {
    return axios.get("/product/find-all").then(
        result => result.data
    )
}

export const findProductName = (name) => {
    return axios.get(`/product/find-name?productName=${name}`).then(
        result => result.data
    )
}

export const findProduct = (id) => {
    return axios.get(`/product/find?productId=${id}`).then(
        result => result.data
    )
}

export const findCategory = (category) => {
    return axios.get(`/product/find-by-category?category=${category}`).then(
        result => result.data
    )
}

export const modifyProduct = (product) => {
    axios.put("/product/modify", product);
}

export const deleteProduct = (id) => {
    axios.delete(`/product/remove?productId=${id}`);
}
