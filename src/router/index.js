import VueRouter from 'vue-router';
import Login from '../components/Login';
import { MemberDetailPage, MemberRegisterPage, MemberModifyPage, 
  ProductRegisterPage, ProductListPage, ProductModifyPage, OrderPage, 
  OrderListPage, OrderModifyPage, SearchPage, ListPage } from '../components/page'

const routes = [
    { path: '/myinfo', component: MemberDetailPage },
    { path: '/login', component: Login },
    { path: '/register', component: MemberRegisterPage },
    { path: '/modify', component: MemberModifyPage },
    { path: '/productRegister', component : ProductRegisterPage },
    { path: '/productListPage', component : ProductListPage },
    { path: '/productModifyPage', component : ProductModifyPage, name: '/productModifyPage', props: true},
    { path: '/orderPage', component : OrderPage, name: '/orderPage', props: true},
    { path: '/orderListPage', component : OrderListPage },
    { path: '/orderModifyPage', component : OrderModifyPage, name: '/orderModifyPage', props: true},
    { path: '/searchPage', component : SearchPage, name: '/searchPage', props: true},
    { path: '/listPage', component : ListPage, name: '/listPage', props: true},
  ];

export default new VueRouter({
    routes: routes
  })